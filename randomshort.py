#!/usr/bin/python3
import string

from webapp import WebApp
import shelve
import text
from urllib import parse
import random

PORT = 5555
content = shelve.open("content")

def show_urls():
    urls = ""
    for element in content:
        urls += "Long Url: {} || Short Url: <a style='color:blue' href='http://localhost:{}{}'>{}</a><br>".format(content[element], PORT, element, element)
    return urls


def full_url(url):
    if not url.startswith('http://') and not url.startswith('https://'):
        url = 'https://' + url
    return url
    
class Shortener(WebApp):
    def parse (self, request):
        """Return the resource name (/ removed)"""

        data = {}
        body_start = request.find('\r\n\r\n')
        if body_start == -1:
            data['body'] = None
        else:
            data['body'] = request[body_start + 4:]
        request_parts = request.split(' ', 2)
        data['method'] = request_parts[0]
        data['resource'] = request_parts[1]
        return data


    def process (self, data):
        """Produce the page with the content for the resource"""

        if data['method'] == 'GET':
            code, page = self.get(data['resource'])
        elif data['method'] == 'PUT':
            code, page = self.put(data['resource'], data['body'])
        elif data['method'] == 'POST':
            code, page = self.post(data['resource'], data['body'])
        else:
            code, page = "405 Method not allowed",\
            text.PAGE_NOT_ALLOWED.format(method=data['method'])
        return (code, page)

    def get(self, resource):
        if resource == '/':
            page = text.PAGE.format(urls=show_urls(), content=content, resource=resource, form=text.FORM)
            code = "200 OK"
        elif resource.startswith('/'):
            short_url = resource[1:]  # Elimina "/"
            if short_url in content:
                long_url = content[short_url]
                page = ""
                code = "301 Moved Permanently\r\nLocation:" + long_url
            else:
                page = text.PAGE_NOT_FOUND.format(urls=show_urls(), resource=resource, form=text.FORM)
                code = "404 Not Found"
        else:
            page = text.PAGE_NOT_FOUND.format(urls=show_urls(), resource=resource, form=text.FORM)
            code = "404 Resource Not Found"
        return (code, page)

    def put(self, resource, body):
        content[resource] = body
        page = text.PAGE.format(urls=show_urls(),content=body, resource=resource, form=text.FORM)
        code = "200 OK"
        return code, page

    def post(self, resource, body):
        fields = parse.parse_qs(body)
        if resource == '/':
            if 'resource' in fields:
                url = full_url(fields['resource'][0])
                if url in content.values():
                    shortened_url = list(content.keys())[list(content.values()).index(url)]
                    content[shortened_url] = url
                else:
                    chars = string.ascii_letters + string.digits
                    shortened_url = ''.join(random.choice(chars) for _ in range(10))
                    content[shortened_url] = url
                page = text.PAGE.format(urls=show_urls(), content=content, resource=resource, form=text.FORM)
                code = "200 OK"
            else:
                page = text.PAGE_UNPROCESSABLE.format(body=body)
                code = "422 Unprocessable Entity"
        else:
            code = "405 Method not allowed"
            page = text.PAGE_NOT_ALLOWED.format(method='POST')
        return code, page


if __name__ == "__main__":
    try:    
        testWebApp = Shortener("localhost", PORT)
    except KeyboardInterrupt:
        print("Bye bye!")


