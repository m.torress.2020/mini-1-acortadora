FORM = """
    <hr>
    <form action="/" method="post">
      <div>
        <label>Resource: </label>
        <input type="text" name="resource" required>
      </div>
      <div>
        <label>Content: </label>
        <textarea name="content"></textarea>
      </div>
      <div>
        <input type="submit" value="Submit">
      </div>
    </form>
"""

PAGE = """
<!DOCTYPE html>
<html>
    <head>
        <title>Content for {resource}: {content}</title>
    </head>
    <body>
        <h1>HELLO</h1>
        <p>URL LIST:<br>{urls}</p>
        <h2>INSTRUCTIONS</h2>
        <p>
            Enter URL to shorten or enter shortened URL.<br>
        </p>
        {form}
    </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
      <p>Resource not found: {resource}.</p>
    </div>
    <div>
      {form}
    </div>
  </body>
</html>
"""

PAGE_NOT_ALLOWED = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Method not allowed: {method}.</p>
  </body>
</html>
"""

PAGE_UNPROCESSABLE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Unprocesable POST: {body}.</p>
  </body>
</html>
"""
