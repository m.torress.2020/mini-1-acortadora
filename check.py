#!/usr/bin/python3
"""
Script de comprobación de entrega de ejercicio

Para ejecutarlo, desde la shell:
 $ python3 check.py login_laboratorio

"""

import os
import random
import sys

ejercicio = 'mini-1-acortadora'

student_files = ['randomshort.py']

optional_files = ['webapp.py']

repo_files = [
    'check.py',
    'README.md',
    '.gitignore',
    'text.py',
    'LICENSE'
    ]

# Obligatory files are a combination of student files and repository files
obligatory_files = student_files + repo_files

# All files include student files, repository files, and optional files
all_files = student_files + repo_files + optional_files

# Minimum number of files expected in the repository
min_files = len(student_files + repo_files)

# Maximum number of files expected in the repository
max_files = len(student_files + repo_files + optional_files)

# Check the command line arguments
if len(sys.argv) != 2:
    print()
    sys.exit("Usage: $ python3 check.py {--local | login_laboratorio}")

# Set up directory and file list based on command line argument
if sys.argv[1] == '--local':
    dir = '.'
    github_file_list = os.listdir(dir)
else:
    aleatorio = str(int(random.random() * 1000000))
    dir = '/tmp/' + aleatorio
    repo_git = "http://gitlab.etsit.urjc.es/" + sys.argv[1] + "/" + ejercicio

    print()
    print("Cloning repository " + repo_git + "\n")
    os.system('git clone ' + repo_git + ' ' + dir + ' > /dev/null 2>&1')

    try:
        github_file_list = os.listdir(dir)
    except OSError:
        error = 1
        print("Error: Could not access repository " + repo_git + ".")
        print()
        sys.exit()

error = 0

# Check if the number of files in the repository is correct
if (len(github_file_list) < min_files) or (len(github_file_list) > max_files):
    error = 1
    print("Error: Incorrect number of files in the repository")

# Check for obligatory files in the repository
for filename in obligatory_files:
    if filename not in github_file_list:
        error = 1
        print("\tError: " + filename + " not found in the repository.")

# Check for unexpected files in the repository
for filename in github_file_list:
    if filename not in all_files:
        error = 1
        print("\tError: " + filename + " should not be in the repository.")

# Print message if no errors detected
if not error:
    print("No errors detected (but be cautious, there might be some).")

print()
print("The pep8 output is: (if everything is fine, it should not show anything)")
print()
for filename in student_files:
    if filename in github_file_list:
        os.system('pycodestyle --repeat --show-source --statistics '
                  + dir + '/' + filename)
    else:
        print("File " + filename + " not found in the repository.")
print()


