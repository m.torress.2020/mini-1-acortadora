import socket


class WebApp:
    """Root of a hierarchy of classes implementing web applications

    This class does almost nothing. Usually, new classes will
    inherit from it, and by redefining "parse" and "process" methods
    will implement the logic of a web application in particular.
    """

    def __init__(self, hostname, port):
        """Initialize the web application."""
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mySocket.bind((hostname, port))

        mySocket.listen(5)

        while True:
            print("Waiting for connections")
            (recvSocket, address) = mySocket.accept()
            print("HTTP request received:")
            request = recvSocket.recv(2048)
            print(request)
            request_parse = self.parse(request.decode('utf8'))
            (code, htmlAnswer) = self.process(request_parse)
            print("Answering back...")
            answer = "HTTP/1.1 " + code + " \r\n\r\n" \
                       + htmlAnswer + "\r\n"
            recvSocket.send(answer.encode('utf8'))
            recvSocket.close()

    def parse(self, request):
        """Parse the received request, extracting the relevant information."""

        return None

    def process(self, request_parse):
        """Process the relevant elements of the request.

        Returns the HTTP code for the reply, and an HTML page.
        """

        return ("200 OK", "<html><body><h1>It works!</h1></body></html>")


